<?php

return [
    'aws_region' => env('AWS_SECRETS_REGION', env('AWS_DEFAULT_REGION', 'us-east-1')),
    'drivers' => [
        'active' => env('SECRETS_DRIVER','database'),
        'aws' => [
            'version' => '2017-10-17',
            'region'  => env('AWS_SECRETS_REGION', env('AWS_DEFAULT_REGION', 'us-east-1')),
            'aws_access_key_id' => env('AWS_ACCESS_KEY_ID'),
            'aws_secret_access_key' => env('AWS_SECRET_ACCESS_KEY'),
        ],

        'database' => env('SECRETS_DB_CONNECTION', config('database.default'))
    ]
];
