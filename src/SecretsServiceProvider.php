<?php

namespace Onemineral\SecretsManager;

use Aws\SecretsManager\SecretsManagerClient;
use Illuminate\Support\ServiceProvider;
use Onemineral\SecretsManager\Contracts\SecretsDriver;
use Onemineral\SecretsManager\Drivers\AwsDriver;
use Onemineral\SecretsManager\Drivers\DatabaseDriver;

class SecretsServiceProvider extends ServiceProvider
{

    public function boot()
    {
        // check if app is running in console mode
        if ($this->app->runningInConsole()) {
            // register the migrations
            $this->loadMigrationsFrom(__DIR__ . '/../database/migrations/');
        }
    }

    public function register()
    {
        $this->publishes([
            __DIR__ .'/../config/secrets.php' => config_path('secrets.php')
        ]);

        $this->mergeConfigFrom(
            __DIR__ . '/../config/secrets.php',
            'secrets'
        );


        $this->app->singleton(SecretsDriver::class, function () {
            $driver = config('secrets.drivers.active');

            switch($driver) {
                case 'database':
                    return new DatabaseDriver();
                case 'aws':
                    $client = new SecretsManagerClient(config('secrets.drivers.aws'));
                    return new AwsDriver($client);
            }
        });

    }

}
