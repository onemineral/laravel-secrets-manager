<?php


namespace Onemineral\SecretsManager\Drivers;


use Onemineral\SecretsManager\Contracts\SecretsDriver;
use Onemineral\SecretsManager\Models\Secret as DbDriver;

class DatabaseDriver implements SecretsDriver
{

    /**
     * @param string $secretName
     * @return mixed|null
     * @throws \Exception
     */
    public function get(string $secretName)
    {
       $result = DbDriver::query()->where('name', '=', $secretName)->get('value')->first();

        if ($result === null) {
            throw new \Exception('Secret not found!');
        }

       return $result->value;
    }

    /**
     * @param string $secretName
     * @param string $secretValue
     * @param string|null $description
     * @return bool
     */
    public function create(string $secretName, string $secretValue, ?string $description): bool
    {
        return (new DbDriver([
            'name' => $secretName,
            'description' => $description,
            'value' => $secretValue
        ]))->save();
    }

    /**
     * @param string $secretName
     * @param string $secretValue
     * @return bool
     * @throws \Exception
     */
    public function update(string $secretName, string $secretValue): bool
    {
        $response = DbDriver::query()->where('name', '=', $secretName)->update(['value' => $secretValue]);

        if ($response !== 1) {
            throw new \Exception('Secret not found');
        }
        return true;
    }

    /**
     * @param string $secretName
     * @return bool
     */
    public function delete(string $secretName): bool
    {
        return DbDriver::query()->where('name', '=', $secretName)->delete();
    }
}