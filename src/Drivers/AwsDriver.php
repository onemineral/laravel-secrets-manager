<?php


namespace Onemineral\SecretsManager\Drivers;


use Aws\SecretsManager\SecretsManagerClient;
use Onemineral\SecretsManager\Contracts\SecretsDriver;

class AwsDriver implements SecretsDriver
{
    /** @var SecretsManagerClient  */
    private $client;

    public function __construct(SecretsManagerClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $secretName
     * @return mixed
     * @throws \Exception
     */
    public function get(string $secretName)
    {
        try {
            $result = $this->client->getSecretValue([
                'SecretId' => $secretName,
            ]);

            if (isset($result['SecretString'])) {
                $secret = $result['SecretString'];
            } else {
                $secret = base64_decode($result['SecretBinary'] ?? null);
            }

            return $secret;

        } catch(\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param string $secretName
     * @param $secretValue
     * @param string|null $description
     * @return bool
     */
    public function create(string $secretName, $secretValue, ?string $description): bool
    {
        $this->client->createSecret([
            'Description'  => $description,
            'Name'         => $secretName,
            'SecretString' => $secretValue,
        ]);

        return true;
    }

    /**
     * @param string $secretName
     * @param $secretValue
     * @return bool
     */
    public function update(string $secretName, $secretValue): bool
    {
        $this->client->putSecretValue([
            'SecretId'     => $secretName,
            'SecretString' => $secretValue,
        ]);

        return true;
    }

    /**
     * @param string $secretName
     * @return bool
     */
    public function delete(string $secretName): bool
    {
        try {
            $this->client->deleteSecret([
                'SecretId' => $secretName
            ]);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}