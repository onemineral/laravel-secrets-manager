<?php

namespace Onemineral\SecretsManager\Facades;

use Illuminate\Support\Facades\Facade;
use Onemineral\SecretsManager\Services\SecretsService;

/**
 * @method static mixed get(string $secretName)
 * @method static void create(string $secretName, $secretValue, $description)
 * @method static void upsert(string $secretName, $secretValue, $description)
 * @method static void update(string $secretName, $secretValue)
 * @method static bool delete(string $secretName)
 *
 * @see \Onemineral\SecretsManager\Services\SecretsService
 */
class Secrets extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return SecretsService::class;
    }

}
