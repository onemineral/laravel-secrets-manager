<?php

namespace Onemineral\SecretsManager\Services;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Onemineral\SecretsManager\Contracts\SecretsDriver;

class SecretsService
{

    /** @var SecretsDriver */
    protected $client;

    /**
     * SecretsService constructor.
     *
     * @param SecretsDriver $client
     */
    public function __construct(SecretsDriver $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $name
     *
     * @return array
     * @throws \Exception
     */
    public function get(string $name)
    {
        try {
            $result = $this->client->get($name);

            return Crypt::decrypt($result);
        } catch(\Exception $e) {
            Log::critical('Secret "' . $name . '" not found', [
                'name' => $name,
                'stack_trace' => debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 50)
            ]);

            throw $e;
        }
    }

    /**
     * @param string $name
     * @param mixed $secret
     *
     * @throws \Exception
     */
    public function update(string $name, $secret)
    {
        try {
            $this->client->update($name,  Crypt::encrypt($secret));
        } catch (\Exception $e) {
            Log::critical($e->getMessage(), [
                'name' => $name,
                'stack_trace' => debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 50)
            ]);

            throw $e;
        }

    }

    /**
     * @param string $name
     * @param string $description
     * @param mixed  $secret
     *
     * @throws \Exception
     */
    public function create(string $name, $secret, string $description)
    {
        try {
            $this->client->create($name, Crypt::encrypt($secret), $description);
        } catch (\Exception $e) {
            Log::critical($e->getMessage(), [
                'name' => $name,
                'stack_trace' => debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 50)
            ]);

            throw $e;
        }

    }

    /**
     * @param string $name
     */
    public function delete(string $name)
    {
        try {
            $this->client->delete($name);

        } catch (\Exception $e) {
            Log::critical($e->getMessage(), [
                'name' => $name,
                'stack_trace' => debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 50)
            ]);

            throw $e;
        }
    }

    /**
     * @param string $name
     * @param $secret
     * @param string $description
     * @throws \Exception
     */
    public function upsert(string $name, $secret, string $description)
    {
        try {
            $this->client->get($name);

            $this->update($name, $secret);
        } catch (\Exception $e) {
            $this->create($name, $secret, $description);
        }
    }

}
