<?php

namespace Onemineral\SecretsManager\Models;

use Illuminate\Database\Eloquent\Model;

class Secret extends Model
{

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->connection = config('secrets.drivers.database');
    }

    protected $fillable = [
        'name', 'description', 'value'
    ];
}