<?php

namespace Onemineral\SecretsManager\Contracts;

interface SecretsDriver
{

    /**
     * @param string $secretName
     * @return mixed
     */
    public function get(string $secretName);

    /**
     * @param string $secretName
     * @param $secretValue
     * @param string|null $description
     * @return mixed
     */
    public function create(string $secretName, string $secretValue, ?string $description): bool;

    /**
     * @param string $secretName
     * @param $secretValue
     * @return mixed
     */
    public function update(string $secretName, string $secretValue): bool;

    /**
     * @param string $secretName
     * @return mixed
     */
    public function delete(string $secretName): bool;

}