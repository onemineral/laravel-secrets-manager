<?php

namespace Tests\Drivers;

use Aws\SecretsManager\SecretsManagerClient;
use Mockery\MockInterface;
use Onemineral\SecretsManager\Drivers\AwsDriver;
use Tests\TestCase;

class AwsDriverTest extends TestCase
{

    /** @var SecretsManagerClient|MockInterface */
    protected $clientMock;

    /** @var AwsDriver  */
    protected $awsDriver;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->clientMock = \Mockery::mock(SecretsManagerClient::class);

        $this->awsDriver = new AwsDriver($this->clientMock);
    }

    public function testCreateSecret()
    {
        $this->clientMock->shouldReceive('createSecret')->with([
            'Description'  => 'description',
            'Name'         => 'name',
            'SecretString' => 'value',
        ])->andReturnTrue();

        $this->awsDriver->create('name', 'value', 'description');
    }

    public function testUpdateSecret()
    {
        $this->clientMock->shouldReceive('putSecretValue')
            ->with([
                'SecretId' => 'name',
                'SecretString' => 'value updated'
            ])
            ->andReturnTrue();

        $this->awsDriver->update('name', 'value updated');
    }

    public function testGetSecret()
    {
        $this->clientMock->shouldReceive('getSecretValue')
            ->with(['SecretId' => 'name'])
            ->andReturn('secret value');

        $this->awsDriver->get('name');
    }

    public function testDeleteSecret()
    {
        $this->clientMock->shouldReceive('deleteSecret')
            ->with(['SecretId' => 'name'])
            ->andReturnTrue();

        $this->awsDriver->delete('name');
    }

}
