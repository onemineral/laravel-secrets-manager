<?php

namespace Tests\Drivers;

use Illuminate\Support\Facades\Crypt;
use Onemineral\SecretsManager\Drivers\DatabaseDriver;
use Onemineral\SecretsManager\Models\Secret;
use Tests\TestCase;

class DatabaseDriverTest extends TestCase
{

    /** @var DatabaseDriver  */
    protected $databaseDriver;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();


        $this->databaseDriver = new DatabaseDriver();
    }

    public function testCreateSecret()
    {
        $this->databaseDriver->create('name', 'value', 'description');
        $this->assertDatabaseHas('secrets', ['name' => 'name', 'value' => 'value']);
    }

    public function testUpdateSecret()
    {
        factory(Secret::class)->create([
            'name' => 'secret name',
            'value' => Crypt::encrypt('secret value')
        ]);

        $this->databaseDriver->update('secret name', 'value updated');
        $this->assertDatabaseHas('secrets', ['name' => 'secret name', 'value' => 'value updated']);
    }

    public function testGetSecret()
    {
        $secret = factory(Secret::class)->create([
            'name' => 'secret name',
            'value' => 'secret value'
        ]);

        $secretString = $this->databaseDriver->get('secret name');
        $this->assertEquals('secret value', $secretString);
    }

    public function testDeleteSecret()
    {
        $this->databaseDriver->delete('name');

        $this->assertDatabaseMissing('secrets', ['name' => 'name']);
    }

}
