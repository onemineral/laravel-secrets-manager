<?php


namespace Tests\Services;


use Onemineral\SecretsManager\Contracts\SecretsDriver;
use Onemineral\SecretsManager\Drivers\DatabaseDriver;
use Onemineral\SecretsManager\Facades\Secrets;
use Onemineral\SecretsManager\Models\Secret;
use Tests\TestCase;

class SecretsServiceTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->app->singleton(SecretsDriver::class, function () {
            return new DatabaseDriver();
        });
    }

    /**
     *
     */
    public function testCreateSecret()
    {
        Secrets::create('secret name', 'secret value', 'description');

        $this->assertDatabaseHas('secrets', ['name' => 'secret name']);
    }

    /**
     *
     */
    public function testGetSecret()
    {
        Secrets::create('secret name', 'secret value', 'description');

        $secret = Secrets::get('secret name');

        $this->assertEquals('secret value', $secret);
    }

    /**
     *
     */
    public function testUpdateSecret()
    {
        $secret = factory(Secret::class)->create();
        Secrets::update($secret->name, 'updated secret value');

        $updated = Secrets::get($secret->name);
        $this->assertEquals('updated secret value', $updated);
    }

    /**
     *
     */
    public function testUpdateSecretFails()
    {
        $secret = factory(Secret::class)->create();
        $this->expectException(\Exception::class);
        Secrets::update('random name', 'updated secret value');
    }

    /**
     *
     */
    public function testDeleteSecret()
    {
        $secret = factory(Secret::class)->create([
            'name' => 'secret name'
        ]);

        Secrets::delete($secret->name);

        $this->assertDatabaseMissing('secrets', ['name' => $secret->name]);

    }

    /**
     *
     */
    public function testUpsertSecret()
    {
        $secret = factory(Secret::class)->create([
            'name' => 'secret name',
            'value' => '234'
        ]);

        Secrets::upsert($secret->name, 'new value', 'Some secret');

        $this->assertEquals('new value', Secrets::get($secret->name));
    }
}