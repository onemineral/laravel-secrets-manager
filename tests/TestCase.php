<?php

namespace Tests;

use Illuminate\Foundation\Application;
use Illuminate\Foundation\Bootstrap\LoadEnvironmentVariables;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Onemineral\SecretsManager\SecretsServiceProvider;
use Orchestra\Testbench\TestCase as Orchestra;

abstract class TestCase extends Orchestra
{
    use RefreshDatabase;
    /**
     * Set up the test case
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->artisan('migrate')->run();
        $this->withFactories(__DIR__ . '/../database/factories');
    }

    /**
     * @param Application $app
     *
     * @return void
     */
    protected function getEnvironmentSetUp($app): void
    {
        $app->useEnvironmentPath(__DIR__ . '/..');
        $app->bootstrapWith([LoadEnvironmentVariables::class]);

        parent::getEnvironmentSetUp($app);

        $this->setUpDatabase($app);

        $app->register(SecretsServiceProvider::class);
    }

    /**
     * @param Application $app
     *
     * @return void
     */
    private function setUpDatabase(Application $app): void
    {
        $app['config']->set('database.default', env('DB_CONNECTION'));
        $app['config']->set('database.connections.' . env('DB_CONNECTION'), [
            'driver' => env('DB_CONNECTION'),
            'database' => env('DB_DATABASE'),
        ]);
    }
}