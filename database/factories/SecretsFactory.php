<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Crypt;
use Onemineral\SecretsManager\Models\Secret;

$factory->define(Secret::class, function (Faker $faker) {
    return [
        'name' => $faker->domainName,
        'description' => $faker->text,
        'value' => Crypt::encrypt($faker->text)
    ];
});
